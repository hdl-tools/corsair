interface ahblite #(
  parameter ADDR_W = 32,
  parameter DATA_W = 32,
  parameter STRB_W = DATA_W/8
) (
  // synthesis translate_off
  input logic clk
  // synthesis translate_on
);

  logic [ADDR_W-1:0] haddr;
  logic              hwrite;
  logic [2:0]        hsize;
  logic [2:0]        hburst;
  logic [3:0]        hprot;
  logic [1:0]        htrans;
  logic              hmastlock;
  logic [DATA_W-1:0] hwdata;
  logic              hready;
  logic [DATA_W-1:0] hrdata;
  logic              hresp;

  modport out (
    input  hready, hrdata, hresp,
    output haddr, hwrite, hsize, hburst, hprot, htrans, hmastlock, hwdata
  );

  modport in (
    input  haddr, hwrite, hsize, hburst, hprot, htrans, hmastlock, hwdata,
    output hready, hrdata, hresp
  );

  // synthesis translate_off

  task master_init;
    haddr     <= 'b0;
    hwrite    <= 'b0;
    hsize     <= 'b0;
    hburst    <= 'b0;
    hprot     <= 'b0;
    htrans    <= 'b0;
    hmastlock <= 'b0;
    hwdata    <= 'b0;
  endtask

  task write(
    logic [ADDR_W-1:0] addr,
    logic [DATA_W-1:0] data,
    logic [STRB_W-1:0] strb = {STRB_W{1'b1}}
  );
  	if (hready == 1'b0) wait(hready == 1'b1);
  	@(posedge clk);
    haddr     <= addr;
    hwrite    <= 'b1;
    hsize     <= 'b010;
    hburst    <= 'b0;
    hprot     <= 'b0001;
    htrans    <= 'b10;
    hmastlock <= 'b0;
    @(posedge clk);
    hwdata    <= data;
    htrans    <= 'b00;
    if (hready == 1'b0) wait(hready == 1'b1);
    @(posedge clk);

  endtask

  task read(logic [ADDR_W-1:0] addr, output logic [DATA_W-1:0] data);

	if (hready == 1'b0) wait(hready == 1'b1);
	@(posedge clk);
    haddr     <= addr;
    hwrite    <= 'b0;
    hsize     <= 'b010;
    hburst    <= 'b0;
    hprot     <= 'b0001;
    htrans    <= 'b10;
    hmastlock <= 'b0;

    @(posedge clk);
    htrans    <= 'b00;

    @(posedge clk);
    if (hready == 1'b0) begin
    	wait(hready == 1'b1);
    end

	data <= hrdata;
	$display("%0t, Read Data: 32'h%x @ 32'h%x", $time, hrdata, addr);

    @(posedge clk);

  endtask

  // synthesis translate_on

endinterface //axilite