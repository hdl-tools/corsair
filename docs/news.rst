.. _news:

===========
What's new?
===========

1.5.1
-----

* Fix `setup.py` to ensure Corsair is used with `Python 3.8`.

1.5.0
-----

* Add `templates_path` option in `crsconfig`.

1.4.2
-----

* Fix `Wavedrom-CSR` packaging.

1.4.1
-----

* Add `wavedrom` to local dependency. Fix single line SVG generation.

1.4.0
-----

* Add `Doxygen` support for `C-Header` template.

1.3.1
-----

* Fixe pure function in AHB-Lite VHDL template.

1.3.0
-----

* Add `image_type` option for ASCIIDoc, Markdown and RST generator to generate `svg` (default), `pdf` of `png` images.
* Add `image_alignment` option for RST generator.

1.2.2
-----

* Set specific wavedrom version for generating multiline bitfield diagrams
* Minor changes in template formatting

1.2.1
-----

* Fix pure function violation in AHB-Lite template

1.2.0
-----

* Add `Symbolator` decorator support for Verilog and VHDL templates

1.1.2
-----

* Remove obsolete assertion in AHB-Lite VHDL template
* Update AHB-Lite docs

1.1.1
-----

* Fix ``--version`` argument call and version file creation

1.1.0
-----

* Add generator for AHB-Lite interface (VHDL only!)
* Add generator for CMSIS SVD
* Add generator for reStructuredText
* Fix bugs in c header
* Fixes hardware option ``a`` with access modes ``ro`` and ``wo``

Changelog before forking
========================

1.0.2 (2021-09-26)
------------------

* Fix overlapping of bitfiled names in rendered images for registers

1.0.1 (2021-09-08)
------------------

* Fix an issue where the input globconfig file was not being applied to generators

1.0.0 (2021-09-03)
------------------

**Reworking the entire project almost from scratch. Lots of breaking changes.**

* New configuration file format (INI)
* New file generation flow (more clear)
* Do refactoring of all core modules
* Add enums
* Add C header generator
* Add Verilog header generator
* Add SystemVerilog package generator
* Embed bus interface (AXI-Lite, APB, Avalon-MM) into a register map
* Add VHDL register map generator
* Add plenty of examples
* Rework of documentation
* Update the tests
* Many minor tweaks and fixes


0.3.0 (2021-02-21)
------------------

* Fix Markdown table row endings.
* Add 'Reserved' bitfields to Markdown.
* Fix installation guides.
* Implement access_strobes attribute for register.
* Implement complementary registers.
* Implement write_lock attribute for register.
* Implement FIFO bitfield modifier.
* Implement AXI-Lite to Local Bus bridge on Verilog.
* Implement Avalon-MM to Local Bus bridge on Verilog.

0.2.0 (2021-01-08)
------------------

* Rework CLI keys
* Fix entry point for CLI
* Add Verilog and Markdown writers for a register map
* Add Local Bus bridge writer
* Implement APB to Local Bus bridge on Verilog
* Setup HDL testing environment
* Setup CI/CD via Github Actions
* Documentation fixes, code prettifying and etc.

0.1.0 (2020-12-16)
------------------

* Setup repository
* Setup documentation
* Setup testing
* Implementation of core classes
* Add support of running from a command line
* Add JSON and YAML readers
* Add JSON and YAML writers

