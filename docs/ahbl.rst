.. _ahbl:

========
AHB-Lite
========

Signals
=======

These signals will be used in the interface of the register map.

============ ===== ========= =========================================================
Signal       Width Direction Description
============ ===== ========= =========================================================
hsel_in      1     input     Slave selection from address decoder
haddr_in     >1    input     read/write address
hwrite_in    1     input     Transfer direction indicator
hsize_in     3     input     Transfer size indicator
hburst_in    3     input     Transfer burst type indicator
hprot_in     4     input     Protection control (not supported yet)
htrans_in    2     input     Transfer type indicator
hmastlock_in 1     input     Locked sequence indicator (not supported yet)
hready_in    1     input     Transfer complete indicator
hdata_in     >1    input     Write data
hdata_q      >1    output    Read data
hreadyout_q  1     output    Transfer finish indicator
hresp_q      1     output    Transfer response
============ ===== ========= =========================================================

.. note::

    Specific bit widths for buses are defined in ``globcfg`` section of a ``csrconfig`` file.
    
Implementation details:

* AMBA® 3 AHB-Lite Slave
* ``hresp_q`` signals are tied to 0 - always ``OKAY``
* ``hprot_in`` and ``hmastlock_in`` signals are not handled
* Valid settings for data width are *8*, *16*, *32*, *64*, *128*, *256*, *512* or *1024*-bits 

Protocol
========

Refer to official ARM documentation: `IHI0033 AMBA® 3 AHB-Lite Protocol <https://developer.arm.com/documentation/ihi0033/a/>`_.
