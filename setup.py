import os
import setuptools
from pkg_resources import parse_version

VERSION = "1.5.1"

local_deps_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'deps')

# Write version into file
VERSION_FILE = 'corsair/_version.py'
with open(VERSION_FILE, 'w') as f:
    f.write("version = '%s'\n" % VERSION)

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

# Install package
setuptools.setup(
    name="corsair",
    version=VERSION,
    author="esynr3z",
    author_email="esynr3z@gmail.com",
    description="Control and Status Register map generator for FPGA/ASIC projects",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hdl-tools/corsair",
    project_urls={
        'Documentation': 'https://hdl-tools.gitlab.io/corsair/'
    },
    packages=setuptools.find_packages(exclude='tests'),
    package_data={'corsair': ['templates/*.j2']},
    entry_points={
        'console_scripts': [
            'corsair = corsair.__main__:main',
        ],
    },
    dependency_links=[
        os.path.join(local_deps_path, 'wavedrom_csr-2.0.3.post2-py2.py3-none-any.whl'),
    ],
    install_requires=[
        'pyyaml',
        'jinja2',
        'wavedrom_csr',
        'cairosvg'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8, <3.9',
)
