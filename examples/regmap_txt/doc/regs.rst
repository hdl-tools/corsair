.. |br| raw:: html

   <br/>

Register map
============

Created with `Corsair <https://gitlab.com/hdl-tools/corsair>`__ v1.3.0.

Conventions
-----------

.. list-table::
   :header-rows: 1

   * - Access mode
     - Description
   * - rw
     - Read and Write
   * - rw1c
     - Read and Write 1 to Clear
   * - rw1s
     - Read and Write 1 to Set
   * - ro
     - Read Only
   * - roc
     - Read Only to Clear
   * - roll
     - Read Only / Latch Low
   * - rolh
     - Read Only / Latch High
   * - wo
     - Write only
   * - wosc
     - Write Only / Self Clear

Register map summary
--------------------

Base address: 0x00000000

.. list-table::
   :header-rows: 1
   :widths: auto

   * - Name
     - Address
     - Description
   * - `DATA <#data>`__
     - 0x0000
     - Data register
   * - `CTRL <#ctrl>`__
     - 0x0004
     - Control register
   * - `STATUS <#status>`__
     - 0x0008
     - Status register
   * - `START <#start>`__
     - 0x0100
     - Start register


DATA
----

Data register

Address offset: 0x0000

Reset value: 0x00000000

.. image:: rst_img/data.svg
   :alt: data
   :align: center

.. list-table::
   :header-rows: 1
   :widths: auto

   * - Name
     - Bits
     - Mode
     - Reset
     - Description
   * - val
     - 31:0
     - rw
     - 0x00000000
     - Value of the register

Back to `Register map <#register-map-summary>`__.


CTRL
----

Control register

Address offset: 0x0004

Reset value: 0x00000100

.. image:: rst_img/ctrl.svg
   :alt: ctrl
   :align: center

.. list-table::
   :header-rows: 1
   :widths: auto

   * - Name
     - Bits
     - Mode
     - Reset
     - Description
   * - --
     - 31:16
     - --
     - 0x0000
     - Reserved
   * - val
     - 15:0
     - rw
     - 0x0100
     - Value of the register

Back to `Register map <#register-map-summary>`__.


STATUS
------

Status register

Address offset: 0x0008

Reset value: 0x00000000

.. image:: rst_img/status.svg
   :alt: status
   :align: center

.. list-table::
   :header-rows: 1
   :widths: auto

   * - Name
     - Bits
     - Mode
     - Reset
     - Description
   * - --
     - 31:8
     - --
     - 0x000000
     - Reserved
   * - val
     - 7:0
     - ro
     - 0x00
     - Value of the register

Back to `Register map <#register-map-summary>`__.


START
-----

Start register

Address offset: 0x0100

Reset value: 0x00000000

.. image:: rst_img/start.svg
   :alt: start
   :align: center

.. list-table::
   :header-rows: 1
   :widths: auto

   * - Name
     - Bits
     - Mode
     - Reset
     - Description
   * - --
     - 31:1
     - --
     - 0x0000000
     - Reserved
   * - val
     - 0
     - wosc
     - 0x0
     - Value of the register

Back to `Register map <#register-map-summary>`__.
