#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Template Search Folder Setup
"""

import os


TMP_FOLDER = "/tmp/corsair_tmp"


def get_template_path(template_name, search_folder):

    if search_folder == "default":
        return None

    abs_search_folder = os.path.abspath(search_folder)
    template_search_path = os.path.join(abs_search_folder, template_name)
    if os.path.exists(template_search_path):
        return abs_search_folder

    print("******************************************************")
    print("*   WARNING: Use Default Templates!                  *")
    print("******************************************************")

    return None
